#!/usr/bin/env -S bash --noprofile --login

# What wouldn't we do for a JArda joke...
if [[ "$(uname)" = "FreeBSD" ]]
then
	date() {
		gdate "$@"
	}

	grep() {
		ggrep "$@"
	}

	sed() {
		gsed "$@"
	}

	seq() {
		gseq "$@"
	}

	stat() {
		gnustat "$@"
	}

	touch() {
		gtouch "$@"
	}

	wc() {
		gwc "$@"
	}

	get_ip() {
		ifconfig -a -u -G lo 2>/dev/null | sed -n 's/.*inet *\([^ ]*\) .*/\1/p' | head -1 2>/dev/null
	}

	is_dedicated_dashboard() {
		test "$(hostname)" = "gentoo" -a "$USER" = "jarda"
	}

	time_synced() {
		ntpq -pn | grep -q '^+[^ ]* *[^ ]* *[0-9] '
	}
else
	get_ip() {
		ip -j r 2>/dev/null | jq -r 'sort_by(.metric) | .[] | select(.dst == "default") | first(.prefsrc)' | head -1 2>/dev/null
	}

	is_dedicated_dashboard() {
		test "$(hostname)" = "raspberrypi"
	}

	time_synced() {
		LC_ALL=C timedatectl status | grep -q 'System clock synchronized: yes'
	}
fi

power_off() {
	/sbin/poweroff &>/dev/null </dev/null
	sudo poweroff &>/dev/null </dev/null
}

# Big speedup possible if we utilise extglob!
# someone just needs to rewrite lot of stuff
shopt -s extglob
tmpdir="$(mktemp -d)"
col_light="$(printf '\033[0;36m')"
col_dark="$(printf '\033[1;36m')"
col_red="$(printf '\033[1;31m')"
col_darkred="$(printf '\033[0;31m')"
col_green="$(printf '\033[1;32m')"
col_orange="$(printf '\033[38;5;208m')"
col_yellow="$(printf '\033[38;5;226m')"
col_gold="$(printf '\033[38;5;190m')"
col_silver="$(printf '\033[38;5;250m')"
col_bronze="$(printf '\033[38;5;178m')"
colstr_start='\033['
colstr_clear='\033[0m'
col_clear="$(printf $colstr_clear)"
countdown="$(date -d "2025-05-20 23:59" "+%s")"
countdown_prefix="Dipl - "
countdown_done="!!! DIPLOMKA !!!"
countdown_fallback="Mon 17:13"
countdown_fallback_prefix="S - "
countdown_fallback_done="!!! Sauna !!!"
countdown_reached=
projects=( "libvirt/libvirt" "libvirt/libvirt-python" "libvirt/libvirt-perl" "MichalPrivoznik/virt-lint" )
proj_names=( $(for p in "${projects[@]}"; do echo "$p" | jq -Rr @uri; done) )

remote_url="$(git remote get-url origin)"
proj_name="$(echo $remote_url | sed 's/\.git$//;s_^.*/\([^/]*/.*\)$_\1_' | jq -Rr @uri)"

project="${projects[0]}"
proj_esc="$(echo $project | jq -Rr @uri)"

# Set console font before requesting number of columns
if [[ "$(hostname)" = "raspberrypi" && -e /usr/share/consolefonts/Uni3-Terminus16.psf.gz ]]
then
	unicode_start Uni3-Terminus24x12
fi

dpms() {
	if [[ -n "$xset_unavailable" ]]; then return; fi

	if type xset >/dev/null 2>&1 && xset dpms >/dev/null 2>&1
	then
		xset -dpms &>/dev/null 2>/dev/null
		xset dpms force on &>/dev/null 2>/dev/null
		# Too disruptive after quit on user desktop
		#xset dpms 36000 36000 36000
	else
		xset_unavailable=1
	fi
}

cpu_temp="/sys/class/thermal/thermal_zone0/hwmon0/temp1_input"
if [[ -d "/sys/class/thermal/" ]]
then
	for i in $(cd /sys/class/thermal/; ls -d thermal_zone*)
	do
		if grep -q -e cpu-thermal -e x86_pkg_temp /sys/class/thermal/$i/type 2>/dev/null
		then
			cpu_temp="/sys/class/thermal/$i/temp"
			break
		fi
	done
fi

non_working_day_check_done=

pipelines_left=1
pipelines_top=1
pipeline_left=40
pipeline_top=1
stonks_left=105
stonks_top=6
weather_now_left=1
weather_now_top=13
weather_left=32
weather_top=9
column_separator="┃ "
jpbistro_top=1
jpbistro_left="${stonks_left}"
jpbistro_columns=2

covid19_top=0
covid19_left=70

ip_top=0
ip_left="${pipeline_left}"

pipeline_width="$(($jpbistro_left-$pipeline_left-1))"
# This might be faster
pipeline_pad=( $(eval "echo {1..$pipeline_width}") )
pipeline_pad="${pipeline_pad[*]//?/ }"
pipeline_pad="${pipeline_pad::$pipeline_width}"

skrolovaci_text_co_vsichni_tak_chcou_furt='Loading...   Loading...   Loading...   Loading...   Loading...   Loading...   Loading...   '
skrolovaci_text_co_vsichni_tak_chcou_furt_offset=0
skrolovaci_text_co_vsichni_tak_chcou_furt_left=10
skrolovaci_text_co_vsichni_tak_chcou_furt_top=8
skrolovaci_text_co_vsichni_tak_chcou_furt_size=70
skrolovaci_text_co_vsichni_tak_chcou_furt_cleared="yes"

progress=( "-" "\\" "|" "/" )
# "
progress_pipelines=0
progress_pipelines_left=0
progress_pipelines_top=0
progress_weather=0
progress_weather_left="$weather_now_left"
progress_weather_top="$weather_top"
progress_stonks=0
progress_stonks_top="$(($stonks_top-1))"
progress_jpbistro=0
progress_jpbistro_left="$jpbistro_left"
progress_jpbistro_top="$((jpbistro_top-1))"

last_printed_time=""

curl_log="$tmpdir/curl_errors.txt"
curl_errors=0

full_redraw=0

# Put all size-dependent calculations into this function
update_sizes() {
    columns="$(tput cols)"
	clear_line="$(for i in $(seq 1 "$columns"); do echo -n " "; done)"
	jpbistro_column_width="$(((${columns}+${#column_separator}-${jpbistro_left})/${jpbistro_columns}-${#column_separator}-1))"
	if [[ "$jpbistro_column_width" -lt 5 ]]
	then
		jpbistro_column_width=5
	fi
	progress_stonks_left="$(($stonks_left+$jpbistro_column_width+${#column_separator}+1))"

	rm -f "$tmpdir/jpbistro_successful"
	check_jpbistro
}

resize_window() {
	full_redraw=1
}

play_cmd="aplay"
if type pw-cat >/dev/null 2>&1
then
	play_cmd="pw-cat -p"
elif type paplay >/dev/null 2>&1
then
	play_cmd="paplay"
else
	play_cmd="aplay -D default:1"
fi

last_modif() {
	stat -c %Y "$0"
}

mytime="$(last_modif)"

lock() {
	local filename="$tmpdir/$1_update"

	if [[ -e "$filename" ]]
	then
		return 1
	fi

	touch "$filename"
}

unlock() {
	rm "$tmpdir/$1_update"
}

print_progress() {
	local name="$1"

	local tmp="progress_$name"
	local progress_cur="${!tmp}"
	local text="Updating $name ... ${progress[$progress_cur]}"

	tmp="progress_${name}_left"
	local left="${!tmp}"

	tmp="progress_${name}_top"
	local top="${!tmp}"

	tput cup "$top" "$left"
	if [[ -e "$tmpdir/${name}_update" ]]
	then
		echo "$text"
		eval "progress_$name=$(((progress_$name+1)%${#progress[@]}))"
	else
		echo "${text//?/ } "
	fi
}

ellipsize() {
	local len="$1"
	local str="$2"

	if [[ "$(LC_ALL=C eval 'echo ${#str}')" -gt "$len" ]]
	then
		eval 'echo "${str::$(($len-3))}..."'
	else
		echo "$str"
	fi
}

translate_status() {
	case "$1" in
		success|null)
			echo "good"
			;;
		failed|cancelled)
			echo "bad"
			;;
	esac
}

cool_curl_wrapper() {
	curl -fs "$@" 2>>"$curl_log"

	# Fsck synchronisation
	if [[ "$?" -eq 22 ]]
	then
		echo "above error occured in request '$*'" >>"$curl_log"
		skrolovaci_text_co_vsichni_tak_chcou_furt="Some curl errors have occurred, see $curl_log for details"
		return 1
	fi

	return 0
}

update_proj_stat() {
	local filename_current="$tmpdir/status_${proj_names[$1]}.current"

	if [[ -z "$2" ]]
	then
		return
	fi

	if [[ -e "$filename_current" ]]
	then
	   echo "$(< "$filename_current")"
	fi
	echo "$2" >"$filename_current"
}

check_self() {
	if [[ -n "$NO_SELFCHECK" ]]; then return; fi

	sha_remote="$(cool_curl_wrapper https://gitlab.com/api/v4/projects/$proj_name/repository/commits/master | jq -r .id 2>/dev/null; exit "${PIPESTATUS[0]}")"
	if [[ "$?" -ne 0 ]]; then return; fi

	sha_local="$(cat .git/refs/heads/master)"

	if [[ "$sha_local" != "$sha_remote" ]]
	then
		git pull -qr 2>/dev/null
	fi
}

wait_for_network() {
	local progress_last=0

	if is_dedicated_dashboard
	then
		while ! time_synced
		do
			tput cup 0 0
			echo "Waiting for network and time ... ${progress[$progress_last]}"
			progress_last="$((($progress_last+1)%${#progress[@]}))"
			sleep .1
		done

		tput cup 0 0
		echo "                                  "
	fi
}

check_failed_boot() {
	if [[ "$(hostname)" != "raspberrypi" ]]
	then
		return
	fi

	if journalctl -b 0 | grep 'CPU[0-9]\+: failed to come online$'
	then
		echo "Rebooting (press Ctrl-C in 10 seconds to cancel)"
		sleep 10 || return
		sudo reboot
	fi
}

maybe_reexec() {
	if [[ "$mytime" -ne "$(last_modif)" ]]
	then
		exec "$0"
	fi
}

quit_with_reason() {
	clear
	echo "$*"

	trap - INT EXIT QUIT TERM
	tput cnorm

	if is_dedicated_dashboard
	then
		#sleep 10
		power_off
	fi

	kill -TERM -$$ >/dev/null 2>&1
	exit
}

get_holiday() {
	local stime
	local etime
	local summary

	while read -r time_from time_to name
	do
		if [[ "$1" -gt "$time_from" && "$1" -lt "$time_to" ]]
		then
			echo "${name}"
			return
		fi
	done <"holidays.txt"

	if [[ "$1" -ge "$(date -d "Dec 24" "+%s")" && "$1" -lt "$(date -d "1 year + Jan 1" "+%s")" ]]
	then
		echo "the end of the year"
		return
	fi
}

check_non_working_day() {
	if [[ -n "$non_working_day_check_done" ]]
	then
		return
	fi

	local holiday=
	local date_now="$(date "+%s")"

	touch -d "1 month ago" "$tmpdir/holiday_timestamp"
	if [[ ! -s "holidays.txt" || "$tmpdir/holiday_timestamp" -nt "holidays.txt" ]]
	then
		cool_curl_wrapper 'https://calendar.google.com/calendar/ical/en.czech%23holiday%40group.v.calendar.google.com/public/basic.ics' >"holidays.ics"
		if [[ "$?" -ne 0 ]]; then return; fi

		grep -E '^(DT(START|END);VALUE=DATE:|DESCRIPTION:|SUMMARY:)' "holidays.ics" |
			while read -r stime
			do
				read -r etime
				read -r description
				read -r summary

				stime="$(echo -n "$stime" | tr -d '\n\r')"
				etime="$(echo -n "$etime" | tr -d '\n\r')"
				description="$(echo -n "$description" | tr -d '\n\r')"
				summary="$(echo -n "$summary" | tr -d '\n\r')"

				local time_from="${stime#DTSTART;VALUE=DATE:}"
				local time_to="${etime#DTEND;VALUE=DATE:}"
				local desc="${description#DESCRIPTION:}"
				local name="${summary#SUMMARY:}"

				if [[ "$time_from" = "$stime" ]]
				then
					echo "invalid start: $stime" >&2
					continue
				fi

				if [[ "$time_to" = "$etime" ]]
				then
					echo "invalid start: $etime" >&2
					continue
				fi

				if [[ "$desc" != "Public holiday" ]]
				then
					continue
				fi

				if [[ "$name" = "$summary" ]]
				then
					echo "invalid summary: $summary" >&2
					continue
				fi

				echo "$(date -d "$time_from" "+%s") $(date -d "$time_to" "+%s") $name"
			done >"holidays.txt"
	fi

	if [[ -z "$NO_NWCHECK" ]]
	then
		if ! time_synced
		then
			return
		fi

		local day="$(date "+%u")"

		if [[ "$day" -eq 6 ]]
		then
			quit_with_reason "Not working on Saturday"
		fi
		if [[ "$day" -eq 7 ]]
		then
			quit_with_reason "Not working on Sunday"
		fi

		holiday="$(get_holiday "$date_now")"
		if [[ -n "$holiday" ]]
		then
			quit_with_reason "It's $holiday, I'm not working today"
		fi
	fi

	if [[ -z "$NO_COUNTDOWN" && "$date_now" -gt "$countdown" ]]
	then
		local weeks=0

		countdown_prefix="$countdown_fallback_prefix"
		countdown_done="$countdown_fallback_done"
		while :
		do
			countdown=$(date -d "$weeks week + $countdown_fallback" "+%s")
			holiday="$(get_holiday "$countdown")"

			if [[ -z "$holiday" ]]
			then
				break
			fi

			weeks="$(($weeks + 1))"
		done
	fi

	non_working_day_check_done="yep"
}

# Add optional parameters to unify all print_offset* functions?
print_offset() {
	local x="$1"
	local y="$2"
	local input="$3"

	if [[ -e "$input" ]]
	then
		while IFS= read -r line
		do
			tput cup "$y" "$x"
			echo -n "$line"
			y="$(($y+1))"
		done <"$input"
	fi
}

print_offset_marquee() {
	local name="$1"
	local tmp

	tmp="${name}_left"
	local x="${!tmp}"
	tmp="${name}_top"
	local y="${!tmp}"
	tmp="${name}_size"
	local size="${!tmp}"
	tmp="${name}_offset"
	local offset="${!tmp}"
	tmp="${name}_cleared"
	local cleared="${!tmp}"
	local text="${!name}"

	if [[ -z "$cleared" ]];
	then
		local clr=( $(eval "echo {1..$size}") )
		clr=${clr[*]//?/ }

		tput cup "$y" "$x"
		echo "${clr::${size}}"

		eval "${name}_cleared=yes"
	fi

	tput cup "$y" "$x"

	local text_trimmed="${text##*( )}"
	local text_trimmed="${text_trimmed%%*( )}"
	if [[ "${#text_trimmed}" -le "${size}" ]]
	then
		echo -n "$text_trimmed"
	else
		text="${text:$offset}${text::$offset}"
		echo "${text::$size}"
		eval "${name}_offset=$((($offset+1)%${#text}))"
	fi
}

print_offset_n_columns() {
	local x="$1"
	local y="$2"
	local cols="$3"
	local w="$4"
	local lines="$5"
	local input="$6"
	local cc=0
	local l=0
	local n=0
	local prefix=""

	if [[ ! -e "$input" ]]; then return; fi

	local lines_per_col="$((($lines+$cols-1)/$cols))"

	while IFS= read -r line
	do
		tput cup "$y" "$x"
		echo -n "$prefix$line"
		y="$(($y+1))"
		l="$(($l+1))"
		n="$(($n+1))"

		if [[ $n -eq $lines ]]
		then
			line=
			break
		fi

		if [[ $l -eq $lines_per_col ]];
		then
			l=0
			x="$(($x+$w+${#prefix}+1))"
			y="$2"
			prefix="${column_separator}"
		fi
	done <"$input"

	# Finish the table to make it look nice
	while [[ "$l" -gt 0 && "$l" -lt "$lines_per_col" ]]
	do
		tput cup "$y" "$x"
		echo -n "$prefix$line"
		y="$(($y+1))"
		l="$(($l+1))"
	done
}

just_quit() {
	sleep .5
	rm -rf "$tmpdir"
	tput cnorm
	if [[ -z "$xset_unavailable" ]]; then xset +dpms; fi
	clear
	exit
}

cleanup() {
	tput cnorm
	if [[ -z "$xset_unavailable" ]]; then xset +dpms; fi
	clear
	kill -TERM -$$ >/dev/null 2>&1
}

print_if_exists() {
	if [[ -s "$1" ]]
	then
		cat "$1"
	fi
}

print_cpu_temp() {
	if [[ -n "$NO_CPUTEMP" ]]; then return; fi

	if [[ -e "$cpu_temp" ]]
	then
		tput cup 0 $((columns-18))
		local temp=$(< "$cpu_temp")
		echo -n "CPU temp: "
		if [[ "$temp" -gt 55000 ]]; then echo -n $col_darkred
		elif [[ "$temp" -gt 50000 ]]; then echo -n $col_red
		elif [[ "$temp" -gt 40000 ]]; then echo -n $col_dark
		else echo -n $col_green
		fi
		local cel="$[temp/1000]"
		local mil="${temp#$cel}"
		echo "$cel.$mil°C "
	fi
}

print_time() {
	if [[ -n "$NO_TIME" ]]; then return; fi

	tput cup 20 0
	local date_now
	if [[ "$#" -lt 1 ]]
	then
		date_now="$(date "+%s")"
	else
		date_now="$1"
	fi

	if [[ "$last_printed_time" = "$date_now" ]]
	then
		cat "$tmpdir/toecho.txt"
		return
	fi

	if [[ -n "$NO_SECONDS" ]]
	then
		local timeformat="%H : %M"
	else
		local d="$(($date_now%2))"
		local timeformat="%H : %M : %S"

		if [[ "$d" -eq 1 ]]
		then
			timeformat="%H | %M | %S"
		fi
	fi

	figlet -d . -f doh2 -W -w "$columns" -c "$(date -d "@$date_now" +"$timeformat")" >"$tmpdir/current.txt"
	sed -n 3,18p "$tmpdir/current.txt" | sed -e "s/░\+/$col_light&/g" -e "s/▒\+/$col_dark&/g" >"$tmpdir/toecho.txt"

	if [[ -e "$tmpdir/aoc-latest.txt" ]]
	then
		echo -ne '\n\n' >>"$tmpdir/toecho.txt"

		if [[ -z "$countdown_cleared" ]]
		then
			echo -ne "$clear_line\n$clear_line\n$clear_line\n$clear_line\n$clear_line\n$clear_line" >>"$tmpdir/toecho.txt"
			echo $'\e[6A' >>"$tmpdir/toecho.txt"
			countdown_cleared="1"
			countdown_reached=
		fi

		head -6 "$tmpdir/aoc-latest.txt" >>"$tmpdir/toecho.txt"
	elif [[ -z "$NO_COUNTDOWN" ]]
	then
		local string="$countdown_prefix"
		countdown_cleared=

		echo -ne '\n\n' >>"$tmpdir/toecho.txt"

		if [[ "$date_now" -ge "$countdown" ]]
		then
			string="$countdown_done"
			if [[ -z "$countdown_reached" ]]
			then
				# Clear the time since the "done" string might be shorter
				echo -ne "$clear_line\n$clear_line\n$clear_line\n$clear_line\n$clear_line\n$clear_line" >>"$tmpdir/toecho.txt"
				echo $'\e[6A' >>"$tmpdir/toecho.txt"
				countdown_reached="1"

				touch "$tmpdir/letsgo"
			fi
		else
			local orig_time="$(($countdown-$date_now))"
			local days="$(date -u -d "@${orig_time}" "+%j")"
			days="$((${days/*(0)/}-1))"

			if [[ "$days" -gt 0 ]]
			then
				string="${string}${days}d   "
			fi

			local countdown_date_format="%H : %M : %S"
			if [[ -n "$NO_SECONDS" ]]
			then
			   countdown_date_format="%H : %M"
			fi

			string="${string}$(date -u -d "@${orig_time}" +"${countdown_date_format}")"

			if [[ "$days" -eq 0 ]]
			then
				string="      ${string}      "
			fi
		fi

		# Don't Ask Me, I Have No Idea Why This Works Either
		figlet -d . -f "ANSI Shadow" -W -w "$columns" -c "$string" | \
			sed "s/█\+/$col_red&$col_darkred/g;6q" >>"$tmpdir/toecho.txt"
	fi

	printf $colstr_clear >>"$tmpdir/toecho.txt"
	cat "$tmpdir/toecho.txt"

	last_printed_time="$date_now"
}

fix_weather() {
	sed s/$'\xca'$'\xbb'/"'"/g\;s/$'\xe2'$'\x9a'$'\xa1'"/${col_yellow}ZZ${col_clear}/g;s/[[:space:]]*$//" "$@"
}

fix_weather_forecast() {
	sed 's/;111m/;90m/g'
}

check_weather() {
	if [[ -n "$NO_WEATHER" ]]; then return; fi

	if ! lock weather; then return; fi

	if type wego >/dev/null 2>&1 && wego | fix_weather >"$tmpdir/pocasi-temp.txt"
	then
		sed -n 3,7p "$tmpdir/pocasi-temp.txt" >"$tmpdir/pocasi-ted.txt.new"
		mv "$tmpdir/pocasi-ted.txt"{.new,}

		sed -n 8,17p "$tmpdir/pocasi-temp.txt" >"$tmpdir/pocasi.txt.new"
		mv "$tmpdir/pocasi.txt"{.new,}
	else
		if cool_curl_wrapper 'wttr.in/Brno?0&Q&M' | fix_weather >"$tmpdir/pocasi-ted.txt.new"
		then
			mv "$tmpdir/pocasi-ted.txt"{.new,}
		fi

		if cool_curl_wrapper 'wttr.in/Brno?Q&M' | fix_weather >"$tmpdir/pocasi-temp.txt"
		then
			sed -n '6,15p' "$tmpdir/pocasi-temp.txt" | \
				fix_weather_forecast >"$tmpdir/pocasi.txt.new"
			mv "$tmpdir/pocasi.txt"{.new,}
		fi
	fi

	unlock weather
}

print_weather() {
	if [[ -n "$NO_WEATHER" ]]; then return; fi

	print_progress weather

	print_offset "$weather_left" "$weather_top" "$tmpdir/pocasi.txt"
	print_offset "$weather_now_left" "$weather_now_top" "$tmpdir/pocasi-ted.txt"
}

play_sound() {
	for snd in "$@"
	do
		$play_cmd "$snd" >/dev/null 2>&1
	done
}

check_pipelines() {
	if [[ -n "$NO_PIPELINES" ]]; then return; fi

	local new_stats=()

	>"$tmpdir/pipelines.txt.new"

	for i in $(seq 0 "$(("${#projects[@]}"-1))")
	do
		local project="${projects[$i]}"
		local status

		status="$(cool_curl_wrapper 'https://gitlab.com/api/v4/projects/'"${proj_names[$i]}"'/pipelines?source={push,web,trigger,schedule,api,external,pipeline,chat,webide,parent_pipeline,ondemand_dast_scan,ondemand_dast_validation}&per_page=1' | sed 's/\]\[/,/g;s/,,\+/,/;s/^\[,\+/[/;s/,\+\]/\]/' | jq -r 'sort_by(.created_at) | reverse | .[0].status'; exit "${PIPESTATUS[0]}")"
		if [[ "$?" -ne 0 ]]
		then
			continue
		fi

		local stat_simple="$(translate_status "$status")"

		{
			printf " $project: "
			case "$stat_simple" in
				good) echo -n "$col_green" ;;
				bad) echo -n "$col_red" ;;
				*) echo -n "$col_dark" ;;
			esac

			echo "$status    $col_clear"
			if [[ "$i" -eq 0 ]]
			then
				echo
			fi
		} >>"$tmpdir/pipelines.txt.new"

		last_stat="$(update_proj_stat "$i" "$stat_simple")"

		case "${last_stat}:${stat_simple}" in
			# good:bad|:bad) startup trombone on old failures
			# good:bad) only new failures
			good:bad|:bad)
				touch "$tmpdir/sad_trombone"
				;;
			bad:good)
				touch "$tmpdir/victory"
				;;
		esac
	done

	mv "$tmpdir/pipelines.txt"{.new,}
}

check_pipeline_new() {
	if [[ -n "$NO_PIPELINE" ]]; then return; fi
	cool_curl_wrapper "https://gitlab.com/api/v4/projects/$proj_esc/events/?action=pushed" | \
		jq -r 'map(select(.push_data.ref == "master"))[0]' >"$tmpdir/event.json"
	if [[ "${PIPESTATUS[0]}" -ne 0 || "${PIPESTATUS[1]}" -ne 0 ]]; then return; fi

	push_author="$(jq -r '.author.name' "$tmpdir/event.json")"
	push_count="$(jq -r '.push_data.commit_count' "$tmpdir/event.json")"
	commit="$(jq -r '.push_data.commit_to' "$tmpdir/event.json")"
	commit_from="$(jq -r '.push_data.commit_from' "$tmpdir/event.json")"

	cool_curl_wrapper "https://gitlab.com/api/v4/projects/$proj_esc/repository/commits/$commit?with_stats=yes" >"$tmpdir/commit.json"
	if [[ "$?" -ne 0 ]]; then return; fi

	commit_author="$(jq -r '.author_name' "$tmpdir/commit.json")"
	commit_date="$(jq -r '.committed_date' "$tmpdir/commit.json")"
	commit_title="$(jq -r '.title' "$tmpdir/commit.json")"
	IFS='
' commit_reviewers_tmp=( $(jq -r .message "$tmpdir/commit.json" | sed -n 's/^Reviewed-by: \(.*\) <[^>]*>$/\1/p') )
	commit_reviewers=""
	line=""
	start="Reviewed by: "
	extras=", et al."

	for i in $(seq 0 "$((${#commit_reviewers_tmp[@]}-1))")
	do
		local cr="${commit_reviewers_tmp[$i]}"

		if [[ "$i" -eq 0 ]]
		then
			commit_reviewers="$start$col_dark$(ellipsize $(($pipeline_width-${#extras}-${#start})) "$cr")"
			line="$start$(ellipsize $(($pipeline_width-${#extras}-${#start})) "$cr")"
			continue
		fi

		maybe="$commit_reviewers, $cr"
		if [[ "${#maybe}" -gt "$pipeline_width" ]]
		then
			commit_reviewers="$commit_reviewers$extras"
			line="$line$extras"
			break
		fi

		commit_reviewers="$maybe"
		line="$line, $cr"
	done
	commit_reviewers="$commit_reviewers$col_clear${pipeline_pad:${#line}}"

	stats_plus="$(jq -r '.stats.additions' "$tmpdir/commit.json")"
	stats_minus="$(jq -r '.stats.deletions' "$tmpdir/commit.json")"

	pipeline="$(jq -r '.last_pipeline.id' "$tmpdir/commit.json")"
	last_pipeline_update="$(jq -r '.last_pipeline.updated_at' "$tmpdir/commit.json")"

	# Implement pagination once we get more jobs than 100
	cool_curl_wrapper "https://gitlab.com/api/v4/projects/$proj_esc/pipelines/$pipeline/jobs?per_page=100" | \
		jq -r '.[] | .status' >"$tmpdir/job_stats.txt"
	if [[ "${PIPESTATUS[0]}" -ne 0 || "${PIPESTATUS[1]}" -ne 0 ]]; then return; fi

	jobs_total="$(grep -c . "$tmpdir/job_stats.txt")"

	jobs_success="$(grep -cxF success "$tmpdir/job_stats.txt")"
	jobs_skipped="$(grep -cxF skipped "$tmpdir/job_stats.txt")"
	jobs_failed="$(grep -cxF failed "$tmpdir/job_stats.txt")"
	status="$(jq -r .last_pipeline.status "$tmpdir/commit.json")"

	stats_total_plus="$stats_plus"
	stats_total_minus="$stats_minus"
	commit="$(jq -r '.parent_ids[0]' "$tmpdir/commit.json")"
	while [[ "$commit_from" != "$commit" ]]
	do
		cool_curl_wrapper "https://gitlab.com/api/v4/projects/$proj_esc/repository/commits/$commit?with_stats=yes" >"$tmpdir/commit.json"
		if [[ "$?" -ne 0 ]]; then break; fi

		stats_total_plus="$(($stats_total_plus+$(jq -r '.stats.additions' "$tmpdir/commit.json")))"
		stats_total_minus="$(($stats_total_minus+$(jq -r '.stats.deletions' "$tmpdir/commit.json")))"
		commit="$(jq -r '.parent_ids[0]' "$tmpdir/commit.json")"
	done

	local line
	local extras
	local pad
	{
		echo "Last pipeline update: $(date -d "$last_pipeline_update" "+%F %R:%S")"

		extras=" (+$stats_plus/-$stats_minus)"
		commit_title="$(ellipsize "$(($pipeline_width-${#extras}))" "$commit_title")"
		line="$commit_title (+$stats_plus/-$stats_minus)"
		echo "$col_dark$commit_title$col_clear ($col_red+$stats_plus$col_clear/$col_green-$stats_minus$col_clear)${pipeline_pad:${#line}}"

		commit_date="$(date -d "$commit_date" "+%F %R:%S")"
		extras="Commit by  pushed at ${commit_date}  " # Two extra spaces for unicode incompatibility
		commit_author="$(ellipsize "$(($pipeline_width-${#extras}))" "$commit_author")"
		line="Commit by $commit_author pushed at $commit_date  " # Two extra spaces for unicode incompatibility
		echo "Commit by $col_dark$commit_author$col_clear pushed at $col_dark$commit_date$col_clear${pipeline_pad:${#line}}"

		# Already ellipsized
		echo "$commit_reviewers"

		extras="Pushed by  in a batch of $push_count commits (+$stats_total_plus/-$stats_total_minus)"
		push_author="$(ellipsize "$(($pipeline_width-${#extras}))" "$push_author")"
		line="Pushed by $push_author in a batch of $push_count commits (+$stats_total_plus/-$stats_total_minus)"
		echo "Pushed by $col_dark$push_author$col_clear in a batch of $push_count commits ($col_red+$stats_total_plus$col_clear/$col_green-$stats_total_minus$col_clear)${pipeline_pad:${#line}}"

		color="$col_dark"

		case "$status" in
			success)
				color="$col_green"
				;;
			failed|cancelled)
				color="$col_red"
				;;
		esac

		echo "Status: $color$status$col_clear ($col_green$jobs_success$col_clear/$col_light$jobs_skipped$col_clear/$col_red$jobs_failed$col_clear out of $jobs_total total)"
	} >"$tmpdir/pipeline.txt.new"
	mv "$tmpdir/pipeline.txt"{.new,}
}

check_pipeline_all() {
	if [[ -n "$NO_PIPELINE" &&  -n "$NO_PIPELINES" ]]; then return; fi

	if [[ -e "$tmpdir/pipelines_update" ]]
	then
		return
	fi

	touch "$tmpdir/pipelines_update"

	check_pipelines
	check_pipeline_new

	rm "$tmpdir/pipelines_update"
}

print_pipeline_all() {
	if [[ -n "$NO_PIPELINE" &&  -n "$NO_PIPELINES" ]]; then return; fi

	print_progress pipelines

	print_offset "$pipeline_left" "$pipeline_top" "$tmpdir/pipeline.txt"
	print_offset "$pipelines_left" "$pipelines_top" "$tmpdir/pipelines.txt"
}

check_stocks() {
	SYMBOLS=(IBM KD NVDA CEZ.PR)
	# borrowed from https://github.com/pstadler/ticker.sh/blob/master/ticker.sh
	# and modified
	FIELDS=(symbol marketState regularMarketPrice regularMarketChange regularMarketChangePercent \
	  preMarketPrice preMarketChange preMarketChangePercent postMarketPrice postMarketChange postMarketChangePercent)
	API_ENDPOINT="https://query1.finance.yahoo.com/v7/finance/quote?lang=en-US&region=US&corsDomain=finance.yahoo.com&crumb=lE2Zk7Dvzji"
	COOKIE='Cookie: A1=d=AQABBFKavGcCEJdpxQZiOu4X9ORdF-fAsIkFEgABCAHlvWfwZ94qb2UBAiAAAAcIS5q8Z7JzLfQ&S=AQAAApPVH974uKALcOrCWQSWORI'
	USER_AGENT='User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:135.0) Gecko/20100101 Firefox/135.0'
	ACCEPT='Accept: application/json'

	if [ -z "$NO_COLOR" ]; then
	  : "${COLOR_BOLD:=\e[1;37m}"
	  : "${COLOR_GREEN:=\e[32m}"
	  : "${COLOR_RED:=\e[31m}"
	  : "${COLOR_RESET:=\e[00m}"
	fi

	symbols=$(IFS=,; echo "${SYMBOLS[*]}")
	fields=$(IFS=,; echo "${FIELDS[*]}")

	results=$(cool_curl_wrapper -H "${COOKIE}" -H "${USER_AGENT}" -H "${ACCEPT}" "$API_ENDPOINT&fields=$fields&symbols=$symbols" \
	  | jq '.quoteResponse .result'; exit "${PIPESTATUS[0]}")
	if [[ "$?" -ne 0 ]]; then return; fi


	query () {
	  echo $results | jq -r ".[] | select(.symbol == \"$1\") | .$2"
	}

	for symbol in $(IFS=' '; echo "${SYMBOLS[*]}" | tr '[:lower:]' '[:upper:]'); do
	  marketState="$(query $symbol 'marketState')"

	  if [ -z $marketState ]; then
		printf 'No results for symbol "%s"\n' $symbol
		continue
	  fi

	  preMarketChange="$(query $symbol 'preMarketChange')"
	  postMarketChange="$(query $symbol 'postMarketChange')"

	  if [ $marketState == "PRE" ] \
		&& [ $preMarketChange != "0" ] \
		&& [ $preMarketChange != "null" ]; then
		nonRegularMarketSign='*'
		price=$(query $symbol 'preMarketPrice')
		diff=$preMarketChange
		percent=$(query $symbol 'preMarketChangePercent')
	  elif [ $marketState != "REGULAR" ] \
		&& [ $postMarketChange != "0" ] \
		&& [ $postMarketChange != "null" ]; then
		nonRegularMarketSign='*'
		price=$(query $symbol 'postMarketPrice')
		diff=$postMarketChange
		percent=$(query $symbol 'postMarketChangePercent')
	  else
		nonRegularMarketSign=''
		price=$(query $symbol 'regularMarketPrice')
		diff=$(query $symbol 'regularMarketChange')
		percent=$(query $symbol 'regularMarketChangePercent')
	  fi

	  if [ "$diff" == "0" ] || [ "$diff" == "0.0" ]; then
		color=
	  elif ( echo "$diff" | grep -q ^- ); then
		color=$COLOR_RED
	  else
		color=$COLOR_GREEN
	  fi

	  if [ "$price" != "null" ]; then
		printf "%-10s$COLOR_BOLD%8.2f$COLOR_RESET" $symbol $price
		printf "$color%10.2f%12s$COLOR_RESET" $diff $(printf "(%.2f%%)" $percent)
		printf " %s\n" "$nonRegularMarketSign"
	  fi
	done
}

check_stock() {
	if [[ -n "$NO_STONKS" ]]; then return; fi
	if [[ -e "$tmpdir/stonks_update" ]]; then return; fi

	touch "$tmpdir/stonks_update"

	check_stocks >"$tmpdir/stocks.txt.new"
	mv "$tmpdir/stocks.txt.new" "$tmpdir/stocks.txt"

	rm "$tmpdir/stonks_update"
}

print_stock() {
	if [[ -n "$NO_STONKS" ]]; then return; fi

	print_progress stonks

	print_offset "$stonks_left" "$stonks_top" "$tmpdir/stocks.txt"
}

check_aoc() {
	if [[ -n "$NO_AOC" ]]; then return; fi
	if ! [[ "${HOME}/aoc.json" -nt "$tmpdir/aoc-latest.txt" ]]; then return; fi
	if [[ -e "$tmpdir/aoc_update" ]]; then return; fi

	touch "$tmpdir/aoc_update"

	echo "          ${col_dark}Advent of Code leaderboard:" >"$tmpdir/aoc-latest.txt.new"
	jq -r \
	   '[.members[] | {name: .name, score:.local_score} | select(.score != 0)] | sort_by(.score) | reverse[] | (.score | tostring) + " " + .name' "${HOME}/aoc.json" | \
		sed -e "1s/^/${col_gold}/;2s/^/${col_silver}/;3s/^/${col_bronze}/;4s/^/${col_light}/;\$s/\$/${col_clear}/" -e 's/^/          /' >>"$tmpdir/aoc-latest.txt.new"
	mv "$tmpdir/aoc-latest.txt.new" "$tmpdir/aoc-latest.txt"

	rm "$tmpdir/aoc_update"
}

check_jpbistro() {
	if [[ -n "$NO_JPBISTRO" ]]; then return; fi
	if [[ -e "$tmpdir/jpbistro_successful" ]]; then return; fi
	if [[ -e "$tmpdir/jpbistro_update" ]]; then return; fi

	touch "$tmpdir/jpbistro_update"

	cool_curl_wrapper "https://www.jpbistro.cz/assets/menu/obed-menu/obed-technopark.pdf" >"$tmpdir/jpbistro.orig.pdf"
	if [[ "$?" -ne 0 ]]; then return; fi

	if ! pdftotext "$tmpdir/jpbistro.orig.pdf"
	then
		rm -f "$tmpdir/jpbistro.orig."{pdf,txt}
		return
	fi

	local today="\\w* $(date +'%-d\.%-m\.%Y')"

	sed -n '/'"$today"'/,/^$/{/^'"$today"'$/d;/^$/d;/^[[:upper:]]/p};/^Týdenní menu/,/^$/{/^Týdenní menu/d;/^$/d;/^[[:upper:]]/p}' "$tmpdir/jpbistro.orig.txt" | sed -e 's/ – [^(]*/ /;s/\(.\{,'"$jpbistro_column_width"'\}\).*/\1/' -e "s/kopr/${col_green}kopr${col_clear}/g" >"$tmpdir/jpbistro.txt.new"

	mv "$tmpdir/jpbistro.txt"{.new,}

	if grep -q "$today" "$tmpdir/jpbistro.txt"
	then
		touch "$tmpdir/jpbistro_successful"
	fi

	rm "$tmpdir/jpbistro_update"
}

print_jpbistro() {
	if [[ -n "$NO_JPBISTRO" ]]; then return; fi

	print_progress jpbistro

	# Ugly temp hack for funny easter egg
	if [[ -e "$tmpdir/jpbistro.txt" ]]
	then
	   local lines="$(wc -l "$tmpdir/jpbistro.txt")"
	   lines="${lines%% *}"
	   if [[ "$lines" -gt 9 ]]
	   then
		   lines=9
	   fi
	   print_offset_n_columns "$jpbistro_left" "$jpbistro_top" "$jpbistro_columns" "$jpbistro_column_width" "$lines" "$tmpdir/jpbistro.txt"
	fi
}

check_covid19() {
	if [[ -e "$tmpdir/covid19.txt" ]]; then return; fi
	if [[ -n "$NO_COVID19" ]]; then return; fi

	cool_curl_wrapper https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/zakladni-prehled.csv >"$tmpdir/zakladni-prehled.csv"
	if [[ "$?" -ne 0 ]]; then return; fi
	if [[ ! -s "$tmpdir/zakladni-prehled.csv" ]]; then return; fi

	awk 'BEGIN { FS = ","; } NR > 1 { print "covid19 (" $1 "): " $9; }' "$tmpdir/zakladni-prehled.csv" > "$tmpdir/covid19.txt"
}

print_covid19() {
	check_covid19 </dev/null > /dev/null &
	if [[ -n "$NO_COVID19" ]]; then return; fi

	print_offset "$covid19_left" "$covid19_top" "$tmpdir/covid19.txt"
}

check_blesk() {
	if [[ "$easter_egg_update" = "done" ]]; then return; fi
	#if [[ -e "$tmpdir/blesk.rss" ]]; then return; fi
	cool_curl_wrapper https://www.blesk.cz/kategorie/7623/export-clanku-do-rss-bez-placeneho-obsahu > "$tmpdir/blesk.rss"
	if [[ "$?" -ne 0 ]]; then return; fi

	grep -Z -a -m 5 "title.*CDATA" "$tmpdir/blesk.rss" | sed "s/.*\[//g;s/\s*\].*/;/g" | tr '\n' ' ' > "$tmpdir/blesk_headlines.txt"
}

copy_blesk_headlines_to_skrolovaci_text_co_vsichni_tak_chcou_furt() {
	if [[ "$easter_egg_update" = "done" ]]; then return; fi
	if [[ -e "$tmpdir/blesk_headlines.txt"  ]]; then
		skrolovaci_text_co_vsichni_tak_chcou_furt=$(cat "$tmpdir/blesk_headlines.txt")
	fi
}


maybe_play_sounds() {
	if [[ ! -e "$tmpdir/pipelines_update" ]]
	then

		if [[ -e "$tmpdir/sad_trombone" ]]
		then
			play_sound sadtrombone.wav &
		elif [[ -e "$tmpdir/victory" ]]
		then
			play_sound victory.wav &
		fi

		rm -f "$tmpdir/"{sad_trombone,victory}

	elif [[ -e "$tmpdir/letsgo" ]]
	then
		play_sound letsgo.wav &
		rm -f "$tmpdir/letsgo"
	fi
}

check_ip() {
	if [[ -n "$NO_IP" ]]; then return; fi

	ip_addr="$(get_ip)"

	echo "IP: ${ip_addr}" >"$tmpdir/ip.txt"
}

print_ip() {
	if [[ -n "$NO_IP" ]]; then return; fi

	print_offset "$ip_left" "$ip_top" "$tmpdir/ip.txt"
}

delay() {
	if [[ "$hostname" != "raspberrypi" ]]
	then
		sleep .01
	fi
}

main() {
	export tmpdir

	shopt -s extglob

	trap cleanup INT QUIT EXIT || exit 1
	trap just_quit TERM || exit 1
	trap resize_window SIGWINCH || exit 1

	tput civis
	dpms

	update_sizes

	clear

	check_failed_boot
	wait_for_network
	check_self
	check_non_working_day

	clear

	print_time
	check_weather </dev/null >/dev/null &
	check_pipeline_all </dev/null >/dev/null &
	check_stock </dev/null >/dev/null &
	check_jpbistro </dev/null > /dev/null &
	check_blesk </dev/null >/dev/null &
	check_ip </dev/null >/dev/null &
	check_aoc </dev/null >/dev/null &

	local last_checked=0

	while delay
	do
		check_non_working_day
		tput civis

		# Run date once, then extract all the crap from it
		local full_time="$(date "+%s:%H:%I:%M:%S")"

		local timestamp="${full_time%%:*}"

		full_time="${full_time#*:}"
		local hrs24="${full_time%%:*}"
		hrs24="${hrs24#0}"

		full_time="${full_time#*:}"
		local hours="${full_time%%:*}"
		hours="${hours#0}"

		full_time="${full_time#*:}"
		local min="${full_time%%:*}"
		min="${min#0}"

		full_time="${full_time#*:}"
		local sec="${full_time%%:*}"
		sec="${sec#0}"

		if [[ "$min" -eq 0 && "$sec" -eq 0 ]]
		then
			# Set console font for the parrot
			if [[ "$(hostname)" = "raspberrypi" && -e /usr/share/consolefonts/Uni3-TerminusBold32x16.psf.gz ]]
			then
				unicode_start Uni3-TerminusBold32x16
			fi

			if type terminal-parrot >/dev/null 2>/dev/null
			then
				play_sound $(yes kuku.wav | head -n $hours) &
				terminal-parrot -loops "$(($hours*2))" -delay 50
			else
				timeout "$(($hours*2))s" curl -s parrot.live &
				play_sound $(yes kuku.wav | head -n $hours)
				kill $!
			fi

			# Set console font back to original
			if [[ "$(hostname)" = "raspberrypi" && -e /usr/share/consolefonts/Uni3-Terminus24x12.psf.gz ]]
			then
				unicode_start Uni3-Terminus24x12
				full_redraw=1
			fi

			check_jpbistro </dev/null >/dev/null &
			check_weather </dev/null >/dev/null &
			check_pipeline_all </dev/null >/dev/null &
		fi

		if [[ "$full_redraw" -eq 1 ]]
		then
			update_sizes
			clear
			full_redraw=0
		fi

		print_covid19
		print_ip
		print_cpu_temp
		print_time "$timestamp"
		print_weather
		print_pipeline_all
		print_stock
		print_jpbistro

		copy_blesk_headlines_to_skrolovaci_text_co_vsichni_tak_chcou_furt

		if [[ -z "$NO_SKROLOVACI_TEXT_CO_VSICHNI_TAK_CHCOU_FURT" ]]
		then
		    print_offset_marquee skrolovaci_text_co_vsichni_tak_chcou_furt
		fi

		maybe_play_sounds

		if [[ "$min" -ne 0 && "$(($sec%30))" -eq 0 && "$last_pipeline_check" -ne "$timestamp" ]]
		then
			check_pipeline_all </dev/null >/dev/null &
			last_pipeline_check="$timestamp"
		fi

		if [[ "$min" -ne 0 && "$sec" -eq 30 && "$last_stock_check" -ne "$timestamp"  ]]
		then
			check_stock </dev/null >/dev/null &
			last_stock_check="$timestamp"
		fi

		if [[ "$min" -ne 0 && $(("$sec"%10)) -eq 5 && "$last_aoc_check" -ne "$timestamp"  ]]
		then
			check_aoc </dev/null >/dev/null &
			last_aoc_check="$timestamp"
		fi

		if [[ "$min" -ne 0 && "$sec" -eq 30 && "$last_ip_check" -ne "$timestamp"  ]]
		then
			dpms </dev/null >/dev/null &
			check_ip </dev/null >/dev/null &
			last_ip_check="$timestamp"
		fi

		if [[ "$min" -ne 0 && "$sec" -eq 30 && "$last_blesk_check" -ne "$timestamp"  ]]
		then
			check_blesk </dev/null >/dev/null &
			last_blesk_check="$timestamp"
		fi

		if [[ "$min" -ne 0 && $(("$sec"%20)) -eq 0 && "$last_self_check" -ne "$timestamp"  ]]
		then
			check_self </dev/null >/dev/null &
			last_self_check="$timestamp"
		fi

		if [[ "$((sec%5))" -eq 0 && "$last_reexec_check" -ne "$timestamp" ]]
		then
			maybe_reexec
			last_reexec_check="$timestamp"
		fi

		if [[ "$hrs24" -eq 19 && "$min" -eq 54 ]]
		then
		    quit_with_reason Shutting down
		fi

	done
}

main "$@"

# Local Variables:
# mode:shell-script
# tab-width: 4
# indent-tabs-mode: t
# End:

# vim: filetype=sh:noexpandtab:shiftwidth=4:tabstop=4:softtabstop=4:textwidth=80
